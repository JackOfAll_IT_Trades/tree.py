
#
# Tree.py:  A Python replacement for the Windows "TREE" command.
#
# This utility adds one thing that the original "TREE" command is sorely missing, however:
# the option to specify what depth you want, i.e. how deep you want the directory tree to go.
#
# [Someday, perhaps, I may also decide to add the ability to do string-matching and also
#  expand directories that match a certain string, even if you've already reached the
#  bottom of the depth parameter.  But that's not going to happen anytime soon... ;-) ]
#


import os
import os.path

import sys
import argparse


TREE_DEBUG_LOG_PDT_ENTER = False
TREE_DEBUG_LOG_PDT_ARGS  = False


def Print_Directory_Tree(tree_path, dest_depth, show_files, ascii_only, this_depth = 0, prefix_str = "") :
	if TREE_DEBUG_LOG_PDT_ENTER:  print("DEBUG: PDT: Print_Directory_Tree() -- Entering...")
	if TREE_DEBUG_LOG_PDT_ARGS:
		PDT_log_args_fmt_str = """DEBUG: PDT:
DEBUG: PDT: tree_path  = <{0}>,
DEBUG: PDT: dest_depth = <{1}>,
DEBUG: PDT: show_files = <{2}>,
DEBUG: PDT: ascii_only = <{3}>
DEBUG: PDT: this_depth = <{4}>
DEBUG: PDT: prefix_str = <{5}>
DEBUG: PDT:"""
		PDT_log_args_message = PDT_log_args_fmt_str.format(	tree_path,
															dest_depth,
															show_files,
															ascii_only,
															this_depth,
															prefix_str )
		print(PDT_log_args_message)
	#
	# END if TREE_DEBUG_LOG_PDT_ARGS: ...

	output_path = tree_path
	if this_depth == 0:
		output_path = os.path.abspath(tree_path)
		print(prefix_str + output_path)
	#
	# END if this_depth == 0: ...

	output_files   = None
	output_subdirs = None

	for walk_root, walk_subdirs, walk_files in os.walk(tree_path) :
		output_files   = walk_files.copy()
		output_subdirs = walk_subdirs.copy()

		# Now that we've copied the data from the walk_subdirs list
		# to another list (for output purposes), erase its contents:
		#
		# Why?  By erasing the walk_subdirs list in the middle of a
		# call to os.walk(), we can make sure that we do *not*
		# recurse into any subdirectories.
		# 
		# This allows us to pick and choose which directories to
		# recurse into, based on depth, pattern matching, or some
		# other criteria.  [Rather than being forced to process
		# *all* of the files and directories under the starting
		# folder, the way os.walk() normally works.]
		#
		walk_subdirs[:] = []
	#
	# END for walk_root, walk_subdirs, walk_files in os.walk(tree_path) : ...

	if show_files:
		if len(output_subdirs) > 0:
			file_indent = "|   " if ascii_only else "│   "
		else:
			file_indent = "    "
		#
		# END if len(output_subdirs) > 0: ... else: ...

		file_prefix_str = prefix_str + file_indent

		for current_file in output_files:
			print(file_prefix_str + current_file)
		#
		# END for current_file in output_files: ...

		print(file_prefix_str)
	#
	# END if show_files: ...


	if len(output_subdirs) > 0:
		for i in range(1, len(output_subdirs)) :
			last_subdir_index = len(output_subdirs) - 1
			subdir_indent = "+---" if ascii_only else "├───"
			if i == last_subdir_index:
				subdir_indent = "\---" if ascii_only else "└───"
			#
			# END if i == last_subdir_index: ...

			subdir_prefix_str = prefix_str + subdir_indent

			current_subdir = output_subdirs[i]

			print(subdir_prefix_str + current_subdir)

			next_depth = this_depth + 1
			if (dest_depth == 0) or (next_depth < dest_depth) :
				next_tree_path = os.path.join(tree_path, current_subdir)

				subdir_prefix_indent_str = "|   " if ascii_only else "│   "

				if i == last_subdir_index: subdir_prefix_indent_str = "    "

				next_prefix_str = prefix_str + subdir_prefix_indent_str

				Print_Directory_Tree(	next_tree_path, dest_depth, show_files, ascii_only,
										next_depth, next_prefix_str )
			#
			# END if (dest_depth == 0) or (next_depth < dest_depth) : ...
		#
		# END for current_file in output_files: ...

	else:
		if this_depth == 0:  print("No subfolders exist")
	#
	# END if len(output_subdirs) > 0: ... else: ...

	return
#
# END Print_Directory_Tree(path, depth, show_files, ascii_only, prefix_string) : ...


if __name__ == "__main__":
	#
	# NOTE:  The syntax of the original is:
	#        "TREE [drive:][path] [/F] [/A]"
	#
	parser = argparse.ArgumentParser()

	parser.add_argument("path", type=str, nargs='?', default=".",
						help="the place to start the directory tree")
	parser.add_argument("-d", "--depth", type=int, default=0,
						help="how deep to go when building the tree")
	parser.add_argument("-f", "--show_files", default=False, action='store_true',
						help="show files (not just directories) in tree")
	parser.add_argument("-a", "--ascii", default=False, action='store_true',
						help="use only ASCII (no extended characters)")

	argv_args  = sys.argv[1:]
#	print("argv = <{0}>, argv_args = <{1}>".format(sys.argv, argv_args))
	argv_lower = [a.lower() for a in argv_args]
	args = parser.parse_args(argv_lower)

	print("")
	Print_Directory_Tree(	args.path,			args.depth,
							args.show_files,	args.ascii )
	print("")
#
# END if __name__ == "__main__": ...
